#include <stdio.h>

#ifndef INPUTSTREAM_H
#define INPUTSTREAM_H

typedef struct InputStream {
    FILE *fp;
    char current;
    int line;
    int column;

    int trackPosition;
    int lastLine;
    int lastColumn;
} InputStream;

InputStream *createInputStream(char *filePath);
int closeInputStream(InputStream *in);
int revertInputStream(InputStream *in, long offset);
void revertInputStreamPositionTracker(InputStream *in);
char nextInInputStream(InputStream *in);

#endif