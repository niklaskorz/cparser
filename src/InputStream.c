#include "InputStream.h"
#include <stdlib.h>

InputStream *createInputStream(char *filePath) {
    FILE *fp = fopen(filePath, "r");
    if (fp == NULL) {
        return NULL;
    }

    InputStream *in = malloc(sizeof(InputStream));
    if (in == NULL) {
        return NULL;
    }

    in->fp = fp;
    in->line = 1;
    in->column = 0;
    in->current = 0;
    in->trackPosition = 1;
    in->lastLine = 0;
    in->lastColumn = 0;

    return in;
}

int closeInputStream(InputStream *in) {
    return fclose(in->fp);
}

long inputStreamOffset(InputStream *in) {
    return ftell(in->fp);
}

int revertInputStream(InputStream *in, long offset) {
    return fseek(in->fp, -offset, SEEK_CUR);
}

char nextInInputStream(InputStream *in) {
    in->current = fgetc(in->fp);

    if (in->trackPosition) {    
        in->lastLine = in->line;
        in->lastColumn = in->column;
        if (in->current == '\n') {
            in->line++;
            in->column = 0;
        } else {
            in->column++;
        }
    }

    return in->current;
}

void revertInputStreamPositionTracker(InputStream *in) {
    in->line = in->lastLine;
    in->column = in->lastColumn;
}
