#include <stdio.h>
#include <stdlib.h>
#include "InputStream.h"
#include "TokenStream.h"

int main() {
    InputStream *in = createInputStream("./example.json");
    if (in == NULL) {
        printf("Failed creating input stream\n");
        return 1;
    }

    Token token;
    while (nextToken(in, &token) != EOF) {
        printf("[%2d:%2d]: ", in->line, in->column);
        switch (token.type) {
            case TOKEN_LBRACE:
                printf("{");
                break;
            case TOKEN_RBRACE:
                printf("}");
                break;
            case TOKEN_COMMA:
                printf(",");
                break;
            case TOKEN_COLON:
                printf(":");
                break;
            case TOKEN_DOUBLE:
                printf("DOUBLE %f", token.value.d);
                break;
            case TOKEN_LONG:
                printf("LONG %ld", token.value.l);
                break;
            case TOKEN_STR:
                printf("STRING %s", token.value.s);
                break;
        }
        printf("\n");
    }

    if (token.type == TOKEN_INVALID) {
        printf("Invalid token '%c' at line %d, column %d.\n", in->current, in->line, in->column);
    }

    if (closeInputStream(in) == EOF) {
        printf("Failed closing input stream\n");
        return 1;
    }
    free(in);
}