#include "TokenStream.h"
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

/* Whitespace */

char skipWhitespace(InputStream *in) {
    while (isspace(in->current)) {
        nextInInputStream(in);
    }
    return in->current;
}

/* Floating point and integer */

int scanNumerics(InputStream *in, long *dst) {
    *dst = 0;
    int i;
    for (i = 0; isdigit(in->current); nextInInputStream(in), i++) {
        *dst = (*dst) * 10 + in->current - '0';
    }
    return i;
}

// Returns 0 if not a number
int scanNumber(InputStream *in, Token *dst) {
    // Numbers must not start with 0
    if (in->current == '0' || !isdigit(in->current)) {
        return 0;
    }

    long lvalue;
    scanNumerics(in, &lvalue);

    // Floating point number
    if (in->current == '.') {
        long mantissa;
        nextInInputStream(in);
        int i = scanNumerics(in, &mantissa);

        // TODO: Find a more elegant way to create the double
        double dvalue = (double) lvalue + (double) mantissa * pow(10.0, (double) -i);
        dst->type = TOKEN_DOUBLE;
        dst->value.d = dvalue;
    } else {
        dst->type = TOKEN_LONG;
        dst->value.l = lvalue;
    }

    // We are at the first character after the last digit now.
    // Revert by 1
    if (revertInputStream(in, 1) != 0) {
        printf("Error: could not revert input stream\n");
        return 0;
    }
    revertInputStreamPositionTracker(in);

    return 1;
}

/* String literals */

long peekStringSize(InputStream *in) {
    char c = in->current;
    if (c != '"') {
        // Not a string, abort
        return -1;
    }

    in->trackPosition = 0;

    long size = 0;
    int skipQuote = 0;
    while ((c = nextInInputStream(in)) != '"' || skipQuote) {
        if (c == EOF) {
            return -1;
        }
        skipQuote = 0;
        size++;
        if (c == '\\') {
            skipQuote = 1;
        } 
    }

    in->trackPosition = 1;

    if (revertInputStream(in, size + 1) != 0) {
        printf("Error: could not revert input stream\n");
        return -1;
    }

    return size;
}

int scanString(InputStream *in, Token *dst) {
    long size = peekStringSize(in);
    if (size < 0) {
        return 0;
    }

    char *str = malloc((size + 1) * sizeof(char));
    long i;
    for (i = 0, nextInInputStream(in); i < size; i++, nextInInputStream(in)) {
        str[i] = in->current;
    }
    str[size] = '\0';

    dst->type = TOKEN_STR;
    dst->value.s = str;

    return 1;
}

/* Returns the next token in an input stream
 * InputStream  *in     The stream to read from
 * Token        *dst    The token to write the result to
 *
 * Returns EOF on stream end or on error
 * dst->type will be set to TOKEN_INVALID if an error occured
 */
int nextToken(InputStream *in, Token *dst) {
    nextInInputStream(in);
    char c = skipWhitespace(in);
    if (c == EOF) {
        return EOF;
    }

    switch (c) {
        case '{':
            dst->type = TOKEN_LBRACE;
            return 0;
        case '}':
            dst->type = TOKEN_RBRACE;
            return 0;
        case ',':
            dst->type = TOKEN_COMMA;
            return 0;
        case ':':
            dst->type = TOKEN_COLON;
            return 0;
    }

    if (scanNumber(in, dst) || scanString(in, dst)) {
        return 0;
    }

    // Invalid token, report
    dst->type = TOKEN_INVALID;
    return EOF;
}
