#ifndef TOKENSTREAM_H
#define TOKENSTREAM_H
#include "InputStream.h"

typedef enum TokenType {
    TOKEN_INVALID,
    TOKEN_LBRACE,
    TOKEN_RBRACE,
    TOKEN_COMMA,
    TOKEN_COLON,
    TOKEN_KW_TRUE,
    TOKEN_KW_FALSE,
    TOKEN_KW_NULL,
    TOKEN_DOUBLE, // Floating point number
    TOKEN_LONG, // Integer number
    TOKEN_STR // String literal
} TokenType;

typedef union TokenValue {
    char *s;
    double d;
    long l;
} TokenValue;

typedef struct Token {
    TokenType type;
    TokenValue value;
} Token;

int nextToken(InputStream *in, Token *dst);

#endif